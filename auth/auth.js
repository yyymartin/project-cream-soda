var jwt = require('jsonwebtoken');

module.exports.auth = (req,res,next) =>{
    const token = req.header('Authorization').replace('Bearer ', '');
    const secret = "12345";
    try{
        let decoded = jwt.verify(token, secret);
        req.member = decoded;
        next();
    }catch(error){
        res.status(401).json({
            message: "You have error problem"
        })
    }
}