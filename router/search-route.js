const express = require('express');
var router = express.Router();
const bonjourScraper = require("../scraper/bonjourScraper.js");
const yohoScraper = require("../scraper/yohoScraper.js");
const list = require("../list.js");
const db = require("../dbFunction.js");
const history = require("../searchHistory");

router.get('/product/keyword/:keyword', async function (req, res) {
    let searchResult = history.searchHistory(encodeURI(req.params.keyword));

    if (searchResult === true) {
        const dbData = await db.findInDb(encodeURI(req.params.keyword));
        return res.json({
            success: true,
            dbData: dbData
        })
    } else {
        db.keywordMode()
        const bonjourUrl = list.bonjourUrl(encodeURI(req.params.keyword));
        const yohoUrl = list.yohoUrl(encodeURI(req.params.keyword));
        const bonjourResult = await bonjourScraper.fetchAllUrls(bonjourUrl, encodeURI(req.params.keyword));
        const yohoResult = await yohoScraper.fetchAllUrls(yohoUrl, encodeURI(req.params.keyword));
        history.setLastUpdateTime(encodeURI(req.params.keyword))
        await db.removeData(encodeURI(req.params.keyword));
        await db.storeInDb([...bonjourResult,...yohoResult]);

        // if (bonjourResult.length > 0) {
        //     await db.storeInDb([...bonjourResult]);
        // } else if (yohoResult.length > 0) {
        //     await db.storeInDb([...yohoResult]);
        // } else {
        //     return res.json({ message: "no data can find" })
        // }

        return res.json({
            bonjourResult: bonjourResult,
            yohoResult: yohoResult,
            dbData: [...bonjourResult, ...yohoResult]
        })
    }
})

router.get('/offer/:offerId', async function (req, res) {

    let offerId = req.params.offerId;

    // console.log(offerId);

    if (offerId) {
        const dbData = await db.findOfferInDb(offerId);
        return res.json({
            success: true,
            dbData: dbData
        })
    } else {
        return res.json({
            message: "Please input OfferId"
        })
    }
})

router.get('/product/catergory/:catergory', async function (req, res) {

    if (req.params.catergory === "fragrance"
        || req.params.catergory === "skincare"
        || req.params.catergory === "makeup") {

        let searchResult = history.searchHistory(req.params.catergory);

        if (searchResult === true) {
            const dbData = await db.findInDb(req.params.catergory);
            return res.json({
                dbData: dbData
            })
        } else {
            db.catergoryMode()
            const bonjourUrlgenCatergory = list.bonjourUrlgenCatergory(req.params.catergory);
            const yohoUrlgenCatergory = list.yohoUrlgenCatergory(req.params.catergory);

            // console.log(bonjourUrlgenCatergory, yohoUrlgenCatergory);

            const bonjourResult = await bonjourScraper.fetchAllUrls(bonjourUrlgenCatergory, req.params.catergory);
            const yohoResult = await yohoScraper.fetchAllUrls(yohoUrlgenCatergory, req.params.catergory);
            history.setLastUpdateTime(req.params.catergory)
            await db.removeData(req.params.catergory);
            await db.storeInDb([...bonjourResult, ...yohoResult]);

            return res.json({
                bonjourResult: bonjourResult,
                yohoResult: yohoResult
            })
        }
    } else {
        return res.json({ error: "cannot find catergory" })
    }
})

//dprice  
router.get('/product/keyword/:keyword/orderbydprice', async function (req, res) {
    const orderByPriceResult = await db.keywordAndorderByprice(encodeURI(req.params.keyword), -1);
    return res.json({
        resultOrderByPrice: orderByPriceResult
    })
})

//aprice
router.get('/product/keyword/:keyword/orderbyaprice', async function (req, res) {
    const orderByPriceResult = await db.keywordAndorderByprice(encodeURI(req.params.keyword), 1);
    return res.json({
        resultOrderByPrice: orderByPriceResult
    })
})

module.exports = router;