const express = require('express');
var router = express.Router();
const db = require("../dbFunction.js");
const multer = require("multer");
const upload = multer({ dest: "./uploadedImage" });


router.get('/offer/:offerId/comment', async function (req, res) {
    let offerId = req.params.offerId;
    if (offerId) {
        const dbData = await db.findCommentInDb(offerId);
        return res.json({
            success: true,
            dbData: dbData
        })
    } else {
        return res.json({
            message: "Please input OfferId"
        })
    }
})

router.post("/member/offer/:offerId/comment", upload.array("image"), function (req, res) {
    console.log(req.member)
    const memberData = req.member;
    const offerId = req.params.offerId;
    const content = req.body.content;
    const datetime = new Date().toLocaleDateString() + " " + new Date().toTimeString();
    const image = req.files[0];

    if (!content) {
        return res.json({
            message: "please write comment on the textbox"
        })
    }

    const searchProductIdResult = db.findKeywordInDb(offerId);

    console.log(12345);
    console.log(req.member);

    if (image) {
        searchProductIdResult.then(function (result) {
            if (result.length >= 1) {
                db.storeCommentInKeywordDb([{
                    postBy: memberData.username,
                    postDate: datetime,
                    content: content,
                    offerId: offerId,
                    image: image.path
                }])

                return res.json({
                    success: true,
                    postBy: memberData.username,
                    postDate: datetime,
                    content: content,
                    offerId: offerId,
                    image: image.path
                })
            } else {
                return res.json({
                    message: "cannot find product Id"
                })
            }
        })
    } else {
        searchProductIdResult.then(function (result) {
            console.log(result);
            if (result.length >= 1) {
                db.storeCommentInKeywordDb([{
                    postBy: memberData.username,
                    postDate: datetime,
                    content: content,
                    offerId: offerId,
                }])

                return res.json({
                    success: true,
                    postBy: memberData.username,
                    postDate: datetime,
                    content: content,
                    offerId: offerId,
                })
            } else {
                return res.json({
                    message: "cannot find product Id"
                })
            }
        })
    }
})

router.delete("/member/delete/comment/:commentId", function (req, res) {
    const commentId = req.params.commentId;
    const memberData = req.member

    if (memberData.level === "member") {
        const searchCommentId = db.findCommentIdInDb(commentId);

        searchCommentId.then(function (result) {
            if (result.length >= 1) {
                if (memberData.username === result[0].postBy) {
                    db.deleteCommentInKeywordDb(commentId);
                    return res.json({
                        success: true
                    })
                } else {
                    return res.json({
                        message: "you are not the member of posting this comment"
                    })
                }
            } else {
                return res.json({
                    success: false,
                    message: "cannot find comment Id"
                })
            }
        })
    } else {
        return res.json({
            message: "you are not member"
        })
    }
})

module.exports = router;
