const express = require('express');
var router = express.Router();
const db = require("../dbFunction.js");
var jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

router.post("/register", async function (req, res) {

    console.log(req.body.username); 

    const username = req.body.username;
    const password = req.body.password;
    const email = req.body.email;
    const secret = "12345";
    const regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    // console.log(username, password);

    const payload = {
        username: username,
        registration: new Date().toISOString(),
        level: "member"
    }


    if (!username || !password || !email) {
        return res.json({
            message: "please input your ac on the input box"
        })
    }

    const searchUserResult = db.findMemberInDb(username);

    searchUserResult.then(function (result) {
        if (result.length >= 1) {
            return res.json({
                message: "your username used"
            })
        } else {
            
            if(!email.match(regexEmail)){
                return res.json({
                    message: "your email format is wrong"
                })
            }

            bcrypt.hash(password, 10, function (err, hash) {

                db.storeMemberInDb([{
                    username: username,
                    password: hash,
                    email: email
                }])

                const token = jwt.sign(payload, secret, { expiresIn: "6h" });

                return res.json({
                    success: true,
                    token: token,
                    username: payload.username
                });
            })
        }
    })
})

router.post("/login", (req, res) => {
    const username = req.body.username;
    const password = req.body.password;
    const secret = "12345";
 
    let loginResult = {
        success: false,
    };

    const payload = {
        username: username,
        registration: new Date().toISOString(),
        level: "member"
    }

    const token = jwt.sign(payload, secret, { expiresIn: "6h" });

    if (!username || !password) {
        return res.json({
            message: "your username used"
        })
    }

    const searchUserResult = db.findMemberInDb(username);

    searchUserResult.then(function (result) {

        // console.log(result);
        // console.log(result[0]);

        if (result.length >= 1) {
            bcrypt.compare(password, result[0].password).then((result) => {
                if (result) {
                    loginResult.success = true;
                    loginResult.token = "login success";
                    loginResult.testOnly = token;
                    loginResult.username = payload.username;
                } else {
                    loginResult.success = false;
                    loginResult.message = "wrong password";
                }
                return res.json(loginResult);
            })
        } else {
            return res.json({
                message: "Cannot find the user"
            })
        }
    })
})

module.exports = router;