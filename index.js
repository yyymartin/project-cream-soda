const express = require('express');
const app = express(); 
const db = require("./dbFunction.js");
app.use(express.json());
app.use("/uploadedImage", express.static('uploadedImage'));
const auth = require("./auth/auth"); 
const commentRoute = require("./router/comment-route");
const memberRoute = require("./router/member-route");
const searchRoute = require("./router/search-route");
app.use("/member", auth.auth);
app.use("/", memberRoute);
app.use("/", searchRoute);
app.use("/", commentRoute);


db.initizeDB().then(() => {
    app.listen(8000, () => {
        console.log("Example app listening on port 8000!");
    });
}).catch(console.error);

