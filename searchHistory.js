const checkTime = require("./checkTime/checkTime.js");

let history = {};

// function getLastUpdateTime(key){
//     return history[key];
// }

function setLastUpdateTime(key){
    history[key] = new Date();
}

function searchHistory(value){
    if(history[value]){
        // if last update time within time
        if(history[value] < checkTime.deadline()){
            console.log(history[value],checkTime.deadline())
            return false;
        } else {
            console.log(history[value],checkTime.deadline())
            return true
        }
    } else {
        return false;
    }
}

module.exports.history = history;
module.exports.setLastUpdateTime = setLastUpdateTime;
module.exports.searchHistory = searchHistory;