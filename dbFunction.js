const { query } = require("express");
const req = require("express/lib/request");
const { MongoClient, ObjectId } = require("mongodb");
const checkTime = require("./checkTime/checkTime.js");
const uri = "mongodb+srv://root:root@cluster0.yciqa.mongodb.net/test";
const client = new MongoClient(uri);
let db = null;

async function initizeDB() {
    try {
        await client.connect();
        db = await client.db("storedResult");
    } catch (error) {
        console.log(error);
    }
}
let catergoryOrKeyword = "";

function catergoryMode() {
    catergoryOrKeyword = "catergory";
}

function keywordMode() {
    catergoryOrKeyword = "keyword";
}

async function storeInDb(params) {

    const dbResult = await db.collection(catergoryOrKeyword).insertMany(params);
    console.log(dbResult);
}

async function findInDb(keyword) {
    const query = { keyword: keyword };
    const cursor = await db.collection(catergoryOrKeyword).find(query);
    const docs = await cursor.toArray();

    console.log(docs)
    return docs
}

async function findOfferInDb(offerId) {
    const query = { _id: ObjectId(offerId) };
    const cursor = await db.collection("keyword").find(query);
    const docs = await cursor.toArray();

    console.log(docs)
    return docs
}

// async function findTimeDiffAndKeyword(keyword){

//     // const query = {lastUpdateTime:{$lt:checkTime.isBefore30sec()}},{keyword:keyword}
//     const cursor = await db.collection("storeResult").find({lastUpdateTime:{$lt:checkTime.isBefore30sec()},keyword:keyword});
//     const docs = await cursor.toArray();

//     console.log(docs)
//     return docs
// }

async function removeData(keyword) {
    const query = { keyword: keyword };
    const result = await db.collection(catergoryOrKeyword).deleteMany(query);
    // console.log(result)
    return result
}

async function keywordAndorderByprice(keyword, orderBy) {
    //db.keyword.aggregate([{$match:{"brandAndProduct":/淨肌護膚潔面乳/}},{$sort:{price:-1}}])
    // console.log(keyword);
    const pipeline = [{ $match: { "brandAndProduct": { $regex: keyword } } }, { $sort: { price: orderBy } }];
    keywordMode();
    const cursor = await db.collection(catergoryOrKeyword).aggregate(pipeline);
    const docs = await cursor.toArray();
    // console.log(docs);
    return docs;
}

async function storeMemberInDb(params) {
    const dbResult = await db.collection("member").insertMany(params);
    console.log(dbResult);
}

async function findMemberInDb(username) {
    const query = { username: username }
    const cursor = await db.collection("member").find(query);
    const docs = await cursor.toArray();

    // console.log(docs);
    return docs;
}

async function findKeywordInDb(id) {
    const query = { _id: ObjectId(id) }
    console.log(query);
    const cursor = await db.collection("keyword").find(query);
    const docs = await cursor.toArray();

    // console.log(docs);
    return docs;
}

async function storeCommentInKeywordDb(params) {
    const dbResult = await db.collection("comment").insertMany(params);
    console.log(dbResult);
}

//same page offerId comment
async function findCommentInDb(offerId) {
    const query = { offerId: offerId }
    const cursor = await db.collection("comment").find(query);
    const docs = await cursor.toArray();

    // console.log(docs);
    return docs;
}

async function findCommentIdInDb(commentId) {
    const query = { _id: ObjectId(commentId) }
    const cursor = await db.collection("comment").find(query);
    const docs = await cursor.toArray();

    // console.log(docs);
    return docs;
}

async function deleteCommentInKeywordDb(commentId) {
    const query = { _id: ObjectId(commentId) }
    const dbResult = await db.collection("comment").deleteOne(query);
    console.log(dbResult);
}

module.exports.initizeDB = initizeDB;
module.exports.storeInDb = storeInDb;
module.exports.findInDb = findInDb;
module.exports.removeData = removeData;
module.exports.catergoryMode = catergoryMode;
module.exports.keywordMode = keywordMode;
module.exports.keywordAndorderByprice = keywordAndorderByprice;
module.exports.storeMemberInDb = storeMemberInDb;
module.exports.findMemberInDb = findMemberInDb;
module.exports.findKeywordInDb = findKeywordInDb;
module.exports.storeCommentInKeywordDb = storeCommentInKeywordDb;
module.exports.deleteCommentInKeywordDb = deleteCommentInKeywordDb;
module.exports.findCommentInDb = findCommentInDb;
module.exports.findOfferInDb = findOfferInDb;
module.exports.findCommentIdInDb = findCommentIdInDb;