const axios = require('axios');
const cheerio = require('cheerio');

async function fetchUrl(url, keyword) {
    let result = [];
    let response = await axios(url);
    const html = response.data
    const $ = cheerio.load(html);

    $(".product-item", html).each(function () {
        const platform = "Yoho";
        const rawBrandAndProduct = $(this).find(".product-name.list").text();
        const fakePrice = $(this).find(".product-shop-price").text();
        const brandAndProduct = rawBrandAndProduct.slice(1, -1);
        const otherPartOfurl = $(this).find("a").attr("href");
        const rawImage = $(this).find("img").attr("original");
        var lastUpdateTime = new Date();
        const frountPartOfUrl = "https://www.yohohongkong.com"

        const price = fakePrice + ".00"; 
        let url = frountPartOfUrl + otherPartOfurl;
        let image = frountPartOfUrl+rawImage

        console.log("IMAGE: "+image)

        result.push({
            platform,
            brandAndProduct,
            price,
            url,
            image,
            keyword,
            lastUpdateTime

        })
    
    });
    return result;

}

async function fetchAllUrls(urlArr, keyword) {
    let result = [];
    for (let i = 0; i < urlArr.length; i++) {
        let urlResult = await fetchUrl(urlArr[i], keyword);
        result = result.concat(urlResult);
    }
    console.log("all")
    return result;
}


module.exports.fetchAllUrls = fetchAllUrls;
