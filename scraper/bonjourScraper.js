const axios = require('axios');
const cheerio = require('cheerio');

async function fetchUrl(url, keyword) {
    let result = [];
    let response = await axios(url);
    const html = response.data
    const $ = cheerio.load(html);

    $(".product-layout", html).each(function () {
        const platform = "Bonjour";
        const fakeBrand = $(this).find('.product-manufacturer').text();
        const productName = $(this).find('.item_name').text();
        const productSize = $(this).find(".item_unit").text();
        const fakePrice = $(this).find("price").text();
        const url = $(this).find("a").attr("href");
        const image = $(this).find("img").attr("data-original")
        const brand = fakeBrand.slice(1, -1);
        var lastUpdateTime = new Date();

        const brandAndProduct = brand + productName + " " +productSize
        const price = "$" + fakePrice

        console.log(image)

        result.push({
            platform,
            brandAndProduct,
            price,
            url,
            keyword,
            image,
            lastUpdateTime
        })
    });

    return result;

}

async function fetchAllUrls(urlArr, keyword) {
    let result = [];
    for (let i = 0; i < urlArr.length; i++) {
        let urlResult = await fetchUrl(urlArr[i], keyword);
        // result = [...result, ...urlResult];
        result = result.concat(urlResult);
    }
    // console.log(result.length)
    return result;
}

//  async function getPage(page,urlArr) {
//     const result = [];

//     if (page < urlArr.length) {
//         await axios(urlArr[page]).then(function (response) {
//             const html = response.data
//             const $ = cheerio.load(html);

//            $(".product-layout", html).each(function () {
//                 const fakeBrand = $(this).find('.product-manufacturer').text();
//                 const productName = $(this).find('.item_name').text();
//                 const productSize = $(this).find(".item_unit").text();
//                 const price = $(this).find("price").text();
//                 const url = $(this).find("a").attr("href");
//                 const brand = fakeBrand.slice(1,-1);

//                 result.push({
//                     brand,
//                     productName,
//                     productSize,
//                     price,
//                     url,   
//                 })    
//             });

//             return getPage(++page,urlArr);
//         })
//     }
//     // console.log(result);
//     return result;
// }

// getPage(0,list.genCatergory("catergory"));


module.exports.fetchAllUrls = fetchAllUrls;
